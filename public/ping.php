<?php
	$sitemapurl = 'http://'.$_SERVER['SERVER_NAME'].'/sitemaps.xml';
	$services = array('google','bing','yandex','baidu');
	foreach ($services as $toping) {
		if ($toping == 'google') {
			$pingurl = 'http://www.google.com/webmasters/sitemaps/ping?sitemap='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		elseif($toping == 'bing') {
			$pingurl = 'http://www.bing.com/webmaster/ping.aspx?siteMap='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
		elseif($toping == 'yandex') {
			$pingurl = 'http://blogs.yandex.ru/pings/?status=success&url='.$sitemapurl;
			$curl_handle=curl_init();
			curl_setopt($curl_handle,CURLOPT_URL,$pingurl);
			curl_setopt($curl_handle, CURLOPT_PROXY, "127.0.0.1:9050");
			curl_setopt($curl_handle, CURLOPT_PROXYTYPE, 7);
			curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
			curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
			$buffer = curl_exec($curl_handle);
			curl_close($curl_handle);
		}
	}
?>
