/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
"use strict";

$(function () {

  //Activate the iCheck Plugin
  $('input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });
  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });


  //bootstrap WYSIHTML5 - text editor
  //$(".textarea").wysihtml5();



  /* jQueryKnob */
  //$(".knob").knob();

  /*fix info box padding */
  //$('span.info-box-icon').css('padding-top','10px');
  $('.box-header.ui-sortable-handle').css('cursor','move');

 




});