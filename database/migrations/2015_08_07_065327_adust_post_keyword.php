<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdustPostKeyword extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            $table->dropColumn('image_url');
            $table->dropColumn('thumb_url'); 
            $table->dropColumn('viewed'); 
            $table->dropColumn('ratingCount');
            $table->dropColumn('ratingValue');
            $table->dropColumn('category'); 
            $table->dropColumn('slug_category');
            $table->dropColumn('category_id'); 
            $table->dropColumn('last_viewed');
            $table->string('code',16)->unique();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_keyword', function (Blueprint $table) {
            //
        });
    }
}
