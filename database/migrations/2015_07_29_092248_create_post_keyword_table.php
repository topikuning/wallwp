<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_keyword', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('title')->index(); 
            $table->text('body');
            $table->string('slug')->unique();
            $table->string('image_url'); 
            $table->string('thumb_url');
            $table->integer('viewed')->index()->unsigned();
            $table->integer('ratingCount')->index();
            $table->integer('ratingValue')->index();
            $table->string('category'); 
            $table->string('slug_category')->index(); 
            $table->timestamps();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->integer('post_id')->index()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_keyword');
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('post_id');
        });
    }
}
