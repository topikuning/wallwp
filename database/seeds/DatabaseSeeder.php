<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('SpinnerSeeder');

        Model::reguard();
    }
}

class SpinnerSeeder extends Seeder {

    public function run()
    {
        DB::table('spinner')->delete();
        $path = base_path() . '/database/seeds/spinner/'; 
        $template = ['alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta', 'eta', 'theta', 'iota', 'kappa'];
        foreach ($template as $key => $value) {
            $file = file_get_contents($path . $value); 
            DB::table('spinner')->insert(['spin_type' => $value, 'text' => $file, 'created_at' => DB::raw('now()')]);
        }
    }

}
