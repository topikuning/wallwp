<?php namespace App\Composers;

use DB;
use stdClass;
use Cache;

class ComposerNavigation {
	
	public function lastView($view)
    {
		$last_viewed = Cache::remember('postsTable_'. env('APP_KEY'), 1440, function()
		{
			return DB::table('posts')
						->select('*', DB::raw('SUM(viewed) as t_viewed'))
						->where('created_at', '<=', DB::raw('now()'))
                        ->groupBy('keyword')
						->orderBy('t_viewed','DESC')
                        ->take(20)
                        ->get();
		});

        $view->with(['last_viewed' => $last_viewed]);
    }

    public function getPage($view)
    {
		$pages = Cache::remember('pagesTable_'. env('APP_KEY'),1440, function()
		{
			return DB::table('pages')
                     ->where('published','1')
                     ->orderBy('title')
                     ->get();
		});
        $view->with(['pages' => $pages]);
    }



}