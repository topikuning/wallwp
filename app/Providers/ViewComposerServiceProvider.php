<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * [boot description]
     * @return [type] [description]
     */
    public function boot()
    {
        $this->composeNavigation();
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
 
    }

    private function composeNavigation()
    {
        view()->composer('partials._navbar', 'App\Composers\ComposerNavigation@getPage');
        view()->composer('pages.main', 'App\Composers\ComposerNavigation@lastView');
    }
}
