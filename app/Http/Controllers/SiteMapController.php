<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Carbon\Carbon;
use Cache;

class SiteMapController extends Controller {
    
    public function index()
    {
        ob_get_clean();
        header('Content-Type: text/xml;charset=utf-8'); 
        echo '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/sitemap-index.xsl').'"?>';
        echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">';
        $data = Cache::remember('sitemapTable_'. env('APP_KEY'), 360, function()
		{
			return DB::table('post_keyword')
						->join('posts', 'post_keyword.id', '=', 'posts.post_id')
						->where('posts.created_at', '<=', DB::raw('now()'))
						->where('post_keyword.created_at', '<=', DB::raw('now()'))
						->orderBy('posts.created_at','DESC')
						->groupBy('post_keyword.id')
						->select('post_keyword.*')
						->take(10000)
						->get();
		});
        $num = 1;
        set_time_limit(0);
        $last_date = false;
        foreach ($data as $key => $row) {
            $url = url('/sitemaps/sitemap-'. $row->code . '.xml'); 
            $lastmod = Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at);
            $datemod = $lastmod->format('Y-m-d\TH:i:sP'); 
            echo '<sitemap><loc>'.$url.'</loc><lastmod>'.$datemod.'</lastmod></sitemap>';
            flush();    
            $num++;
            //$lastmod = $lastmod->subMinutes(1);
        }
        echo '</sitemapindex>';   
    }


    public function image()
    {
        $now = Carbon::now()->subHour();

        $post = DB::table('posts')
                ->where('created_at', '<=', DB::raw('now()'))
                ->orderBy('created_at','DESC')->first();
        if($post){
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        }

        ob_get_clean();
        header('Content-Type: text/xml;charset=utf-8'); 
        echo '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/sitemap-index.xsl').'"?>';
        echo '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">';
        set_time_limit(0);

        $page = 1; 
        $limit = 200; 
        $is_done = false;
        $data =  DB::table('posts')->where('created_at', '<=', DB::raw('now()'))->count();  

        $subtract = $data / $limit;
        $now = $now->subMinutes(intval($subtract));            

        while (!$is_done) {
           $offset = $page * $limit;
           $fakeoffset = $offset;
           $offset = $offset -1;
           if($page ==1) $offset = '0'; 
            $url = url('sitemap/image-'. $page . '.xml'); 
            $datemod = $now->format('Y-m-d\TH:i:sP');
            echo '<sitemap><loc>'.$url.'</loc><lastmod>'.$datemod.'</lastmod></sitemap>';
            flush();         
            if($fakeoffset >= $data) $is_done = true; 
            $now->addMinute();
            $page++;
             
         } 

        echo '</sitemapindex>';

    }

    public function posts($code)
    {
        $lastmod = Carbon::now()->subMinutes(10);
        
		$keyword = Cache::remember('sitemapTable_'.$code. env('APP_KEY'), 1440, function() use ($code)
		{
			return DB::table('post_keyword')->where('code', $code)->first(); 
		});
        if(!$keyword) return ""; 
		
		$kid=$keyword->id;
		$posts = Cache::remember('sitemapPosts_'.$kid. env('APP_KEY'), 1440, function() use ($kid)
		{
			return DB::table('posts')->where('post_id', $kid)->where('created_at', '<=', DB::raw('now()')) ->orderBy('id')->get();
		});

        $content = view('sitemap_post',[
          'lastmod' => $lastmod->format('Y-m-d\TH:i:sP'),
          'keyword' => $keyword,
          'posts' => $posts
        ])->render();



        return response($content)
                        ->header('Content-Encoding','UTF-8')
                        ->header('Content-type','text/xml;charset=utf-8');


        
    }

    public function images($page)
    {


        $page = is_numeric($page)?$page:1; 
        $limit = 200;
        $offset = ($page-1) * $limit;
        $data = DB::table('posts')
                ->where('created_at', '<=', DB::raw('now()'))
                ->orderBy('id','ASC')
                ->skip($offset)
                ->take($limit)
                ->get();


        $content = view('sitemap_image',[
          'posts' => $data
        ])->render();


        return response($content)
                        ->header('Content-Encoding','UTF-8')
                        ->header('Content-type','text/xml;charset=utf-8');

    }

   
}