<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Libs\Superspin\SuperSpin;
use Jenssegers\Agent\Agent;
use Cache;

class HomeController extends Controller {

    public function index($page=1)
    {
		$result=[];
        return view('pages.main', $result);

    }

    public function searchData(Request $request)
    {
        $q = $request->input('s'); 
        $q = trim($q); 
        if(!$q) return Redirect::to($this->index()); 

        $posts = DB::table('posts')
					->where('created_at', '<=', DB::raw('now()'))
                    ->whereRaw("MATCH(title,keyword) AGAINST(? IN BOOLEAN MODE)", array($q))
					->orderBy('title') 
                    ->paginate(12);
					
		DB::table('search')->insert([
								['query' => $q]
							]);
							
		if(!count($posts)){
            return Redirect::to($this->index());
        }
        $page = ' page '. $posts->currentPage();

        return view('pages.search',[
                'posts' => $posts,
                'search' => $q,
                'current_title' => 'Search ' . $q . $page,
                'no_description' => true
            ]);
    }


    public function getSingle($slug)
    {
		$agent = new Agent();
		$mobile = $agent->isMobile();
		$isrobot = $agent->isRobot();
        $code = explode('-', $slug);
		$id = count($code) - 1;
		$sid = $code[$id];
		$post = Cache::remember('singlePosts_'. $sid . env('APP_KEY'), 1440, function() use ($sid)
		{
			return DB::table('posts')
                ->where('created_at', '<=', DB::raw('now()'))
                ->where('id', $sid)
                ->first();
		});
        if(!$post) return Redirect::to($this->index()); 
        $post->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at);
        //if slow dont update the db 
        $statement = 'UPDATE posts SET viewed = viewed +1 WHERE id = '. $post->id . ' LIMIT 1'; 
        if(!$isrobot) DB::statement($statement);

        $post_random = explode(',', $post->post_random);

       $details = [];
       if($post->post_random){
		$randome = $post->post_random;
		$postid = $post->id;
		$details = Cache::remember('randomPosts_'. $post->code . env('APP_KEY'), 1440, function() use ($postid,$randome)
		{
			$query = 'SELECT * FROM posts WHERE created_at<=now() AND id != '.$postid.' AND id IN('.$randome.') ORDER BY FIELD(id, '.$randome.')';
			return DB::select($query);
		});
       }
        $body = [];
        $desc = [];

        foreach ($details as $key => $value) {
            if($key < 4){
                $body[] = $value->title;
            } 
            if($key < 3){
                $desc = $body;
            }       
        }

        $now = Carbon::now();
		
		//RELATED
		$parent = $post->title;
		$related = Cache::remember('relatedPosts_'. $post->code . env('APP_KEY'), 1440, function() use ($parent)
		{
			return DB::table('posts')
                    ->whereRaw("MATCH(title,keyword) AGAINST(? IN BOOLEAN MODE)", array($parent))
                    ->take(10)->get(); 
		});
		if(!count($related)){
            $related = $details;
        }
        $tmp = []; 
        foreach($related as $row){
            $created_at = Carbon::createFromFormat('Y-m-d H:i:s',$row->created_at); 
            if($created_at->lte($now)){
                $tmp[] = $row;
            }
        }

        return view('pages.single',
            ['post' => $post,
              'body' => $body,
             'details' => $tmp,
			 'tags' => $details,
             'current_title' => $post->title .', '. $post->keyword,
             'current_description' => ucfirst(strtolower(implode(' ', $desc))) .'. '. config('site.site_title'),
			 'mobile' => $mobile,
			 'robot' => $isrobot
            ]);
    }

    public function getPage($slug)
    {
		$data = Cache::remember('singlePage_'. $slug . env('APP_KEY'), 1440, function() use ($slug)
		{
			return DB::table('pages')
                    ->where('published','1')
                    ->where('slug', $slug)
                    ->first(); 
		});
        if(!$data) return Redirect::to($this->index());

        return view('pages.page',
            ['page' => $data,
             'current_title' =>$data->title,
             'noindex' => true,
             'current_description' => str_limit($data->body,50)
            ]);
    }
}