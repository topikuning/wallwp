<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB; 


class DashBoardController extends Controller {

    public function index(){
        $data = [
            'countpage' => DB::table('pages')->count(),
            'recent_page' => DB::table('pages')->get()
        ];
        return view('admin.dashboard', $data);
    }
}
