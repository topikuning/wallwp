<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use Validator;
use stdClass;
use Session;


class PageController extends Controller {


    private function setNavigation()
    {
        $navigation = ['parent' => 'pages', 'child' => 'pages'];
        $data = ['navigation' => $navigation];
        return $data;
    }


    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $data = $this->setNavigation();
        return view('admin.page.index', $data);
    }

    /**
     * [read description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function read(Request $request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start'); 
        $length = $request->input('length'); 
        $order = $request->input('order');
        $search = $request->input('search');


        $columns = [
            'id',
            'published',
            'title',
            'slug'
        ];




        $orderField = $columns[$order[0]['column']]; 
        $oderDirect = $order[0]['dir']; 

        $table = DB::table('pages')->select(
                'id',
                'published',
                'title',
                'slug')->orderBy($orderField,$oderDirect); 

        if(trim($search['value'])){
            $index =0;
            foreach($columns as $column){
                if(!$index){
                    $table = $table->where($column, 'LIKE', '%'.  $search['value'] .'%');
                }else{
                    $table = $table->orWhere($column, 'LIKE', '%'.  $search['value'] .'%');
                }

                $index++;
            }
        }

        $data = $table->skip($start)->take($length)->get();
        $arr = []; 
        foreach ($data as $key => $value) {
            $tmp = [];
            foreach($value as $item){
                $tmp[] = $item;
            }
            $tmp[] = '<span class="badge bg-green edit-inline" data-id="'.$value->id.'">Edit</span>'.
                     '<span class="badge bg-red delete-inline" data-id="'.$value->id.'">Delete</span>';
            $arr[] = $tmp;
        }


        return [
            'draw' => $draw,
            'recordsTotal' => $table->count(),
            'recordsFiltered' =>$table->count(),
            'data' => $arr
        ];
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create()
    {
        $data = $this->setNavigation();
        $record = new stdClass;
        $record->id = '';
        $record->title = ''; 
        $record->slug = ''; 
        $record->body = '';
        $data['record'] = $record;
        return view('admin.page.create', $data);

    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
        $record = DB::table('pages')->where('id',$id)->first();
        if(!$record) return redirect('/badmin/posts');
        $data = $this->setNavigation();
        $data['record'] = $record;
        return view('admin.page.create', $data);
    }    

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {
        $filter = [
            'title' => 'required'
        ];

        if(!$request->input('id'))
            $filter['slug'] = 'required|unique:pages';

        $v = Validator::make($request->all(), $filter);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput($request->all());
        }

        $insert = [
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'published' => $request->input('submit')?'1':'0'
        ];

        if($request->input('id')){
            $id = $request->input('id'); 
            $insert['updated_at'] = DB::raw('now()');
            DB::table('pages')->where('id', $id)->update($insert);
            $message = 'Edit Page success';

        }else{
            $insert['slug'] = str_slug($request->input('slug'));
             $insert['created_at'] = DB::raw('now()');
            $id = DB::table('pages')->insertGetId($insert);
            $message = 'New Page was created';
        }

        Session::flash('message',$message);
        if($request->input('id')){
            return redirect()->back();   
        }
        return redirect('/badmin/pages');

    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $ok = DB::table('pages')->where('id', $id)->delete(); 
        return $ok;
    }

    /**
     * [postEditAtrribute description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function postEditAtrribute(Request $request)
    {
        $id = $request->input('id'); 
        $field = $request->input('field'); 
        $value = $request->input('value'); 
        $ok = DB::table('pages')->where('id',$id)->update(
            [$field => $value]
        ); 

        return $ok;
    }



}