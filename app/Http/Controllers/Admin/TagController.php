<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FuncCollection;
use DB;
use stdClass;


class TagController extends Controller {


    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        $navigation = ['parent' => 'master', 'child' => 'tags'];
        $data = ['navigation' => $navigation];
        return view('admin.tag', $data);
    }

    /**
     * read table to json
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function read(Request $request)
    {
        $draw = $request->input('draw');
        $start = $request->input('start'); 
        $length = $request->input('length'); 
        $order = $request->input('order');
        $search = $request->input('search');


        $columns = [
            'id',
            'name',
            'slug'
        ];


        $orderField = $columns[$order[0]['column']]; 
        $oderDirect = $order[0]['dir']; 

        $table = DB::table('tags')->select('id','name','slug')->orderBy($orderField,$oderDirect); 

        if(trim($search['value'])){
            $index =0;
            foreach($columns as $column){
                if(!$index){
                    $table = $table->where($column, 'LIKE', '%'.  $search['value'] .'%');
                }else{
                    $table = $table->orWhere($column, 'LIKE', '%'.  $search['value'] .'%');
                }

                $index++;
            }
        }

        $data = $table->skip($start)->take($length)->get();
        $arr = []; 
        foreach ($data as $key => $value) {
            $tmp = [];
            foreach($value as $item){
                $tmp[] = $item;
            }
            $tmp[] = '<span class="badge bg-green edit-inline">Edit</span>'.
                     '<span class="badge bg-red delete-inline">Delete</span>';
            $arr[] = $tmp;
        }


        return [
            'draw' => $draw,
            'recordsTotal' => $table->count(),
            'recordsFiltered' =>$table->count(),
            'data' => $arr
        ];
    }


    /**
     * [createSlug description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    public function createSlug(Request $request)
    {
        $text = $request->input('text');
        if(trim($text)){
            return str_slug($text);
        }

        return "";
    }

    /**
     * [getCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getCreate(Request $request)
    {
        $record = new stdClass(); 
        $record->id = "";
        $record->name = ""; 
        $record->slug = ""; 
        return view('partials.tag_form',['record' => $record]);
    }

    /**
     * [postCreate description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreate(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required|unique:tags'
        ]);

        $data = $request->all(); 
        $data['slug'] = str_slug($data['slug']); 
        $data['created_at'] = DB::raw('now()');

        DB::table('tags')->insert($data); 
        $tag = DB::table('tags')->orderBy('id','DESC')->first(); 

        return json_encode($tag);


    }

    public function postEdit(Request $request, $id){
         $this->validate($request, [
            'name' => 'required',
            //'slug' => 'required|unique:tags'
        ]);

        $data = $request->all(); 
        //$data['slug'] = str_slug($data['slug']); 
        $data['updated_at'] = DB::raw('now()');  

        DB::table('tags')->where('id',$id)->update($data); 

        return json_encode(DB::table('tags')->where('id',$id)->first()); 

    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        $data = DB::table('tags')->where('id', $id)->delete();  
        return $data;

    }

    public function getEdit($id){
        $data = DB::table('tags')->where('id', $id)->first(); 
        return view('partials.tag_form', ['record' => $data]);
    }
}
