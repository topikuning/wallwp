<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB; 
use Storage;

class UpdateSiteMap extends Command {

  protected $name = 'sitemap:update';

  protected $description = 'Update sitemap website manually';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run update sitemap..");
    $this->generateSiteMap(); 
    $this->info("sitemap has been updated");
  }

  public function generateSiteMap(){    
        $last_id = 0; 
        $last_table_id = 0; 

        if(Storage::exists('last_sitemap')){
          $last_id = Storage::get('last_sitemap');
          $last_id = intval($last_id);
        }

        $reset = $this->argument('reset');
        if($reset){
          $this->info('reset the sitemap'); 
          $last_id = 0;
        }

        $last_table = DB::table('posts')->select('id')->orderBy('id','DESC')->first(); 
        if($last_table) $last_table_id = $last_table->id;
        $content = ""; 
        if(!$last_id){
          $content = $this->save_sitemap();
          $this->info('write to sitemap.xml');
          Storage::put('sitemap.xml', $content);
          Storage::put('last_sitemap', $last_table_id);
        }else{
            if($last_table_id > $last_id){
                $content = $this->save_sitemap(); 
                $this->info('write to sitemap.xml');
                Storage::put('sitemap.xml', $content);
                Storage::put('last_sitemap', $last_table_id); 
            }
        }

  }

  private function createSubSitemap($list)
  {
    $name = 'sitemap-main.xml.gz';
    $this->info('create '. $name);  
    $list[] = ['name' =>$name];
    return $list;
  }


  private function updatePost($list)
  {
        $this->info('update posts');

        $page = 1; 
        $limit = 10000; 
        $is_done = false;


        $data =  DB::table('post_keyword')->count();
        

        while (!$is_done) {
           $offset = $page * $limit;
           $fakeoffset = $offset;
           $offset = $offset -1;
           if($page ==1) $offset = '0'; 
            $this->info('skip '. $offset . ' limit '. $limit);
            $name = 'sitemap-posts-'. $page . '.xml.gz'; 
            $this->info('adding '. $name);
            $list[] = ['name' =>$name];
            if($fakeoffset >= $data) $is_done = true; 
            $page++; 
         } 

        $this->info('akhirnya selese update posts');
        return $list;
  }

  private function save_sitemap()
  {
        
        $list = [];
        $list = $this->createSubSitemap($list);
        $list = $this->updatePost($list);
        $content = view('mainsitemap', ['list' => $list])->render();
        return $content;
  }

  protected function getArguments()
  {
    return [
      ['reset', InputArgument::OPTIONAL, 
        'reset sitemap', null],
    ];
  }

  protected function getOptions()
  {
    return [
      ['example', null, InputOption::VALUE_OPTIONAL, 
        'An example option.', null],
    ];
  }

}