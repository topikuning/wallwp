server {
	listen 80;
	server_name nama.situs;
	return 301 http://www.nama.situs$request_uri;
}

server {
	listen       80;
	server_name  www.nama.situs;
	access_log /var/log/nginx/nama.situs.access.log;
	error_log /var/log/nginx/nama.situs.error.log;

	location / {
		rewrite ^/index.php/(.*) /$1  permanent;
		root   /web/nama.situs/public;
		index  index.php index.html index.htm;
		try_files $uri $uri/ /index.php?$query_string;
	}

	error_page  404              /404.html;
	location = /404.html {
		root   /usr/share/nginx/html;
	}

	# redirect server error pages to the static page /50x.html
	#
	error_page   500 502 503 504  /50x.html;
	location = /50x.html {
		root   /usr/share/nginx/html;
	}

	# proxy the PHP scripts to Apache listening on 127.0.0.1:80
	#
	location ~ \.php$ {
		root           /web/nama.situs/public;   # Document root
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_read_timeout 120;
		fastcgi_index  index.php;
		fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
		include        fastcgi_params;
	}

	# deny access to .htaccess files, if Apache's document root
	# concurs with nginx's one
	#
	location ~ /\.ht {
		deny  all;
	}

	# Remove trailing slash to please routing system.
	if (!-d $request_filename) {
		rewrite     ^/(.+)/$ /$1 permanent;
	}

	# Set header expirations on per-project basis but we remove image cache because image is dynamic
	location ~* \.(?:ico|css|js|svg|woff)$ {
	   root /web/nama.situs/public; 
	   expires 30d;
	   access_log off;
	   add_header Cache-Control "public, must-revalidate, proxy-revalidate";
	}
	
	    set_real_ip_from 103.21.244.0/22;
        set_real_ip_from 103.22.200.0/22;
        set_real_ip_from 103.31.4.0/22;
        set_real_ip_from 104.16.0.0/12;
        set_real_ip_from 108.162.192.0/18;
        set_real_ip_from 131.0.72.0/22;
        set_real_ip_from 141.101.64.0/18;
        set_real_ip_from 162.158.0.0/15;
        set_real_ip_from 172.64.0.0/13;
        set_real_ip_from 173.245.48.0/20;
        set_real_ip_from 188.114.96.0/20;
        set_real_ip_from 190.93.240.0/20;
        set_real_ip_from 197.234.240.0/22;
        set_real_ip_from 198.41.128.0/17;
        set_real_ip_from 199.27.128.0/21;
        set_real_ip_from 2400:cb00::/32;
        set_real_ip_from 2405:8100::/32;
        set_real_ip_from 2405:b500::/32;
        set_real_ip_from 2606:4700::/32;
        set_real_ip_from 2803:f800::/32;
        set_real_ip_from 127.0.0.1/32;
        real_ip_header CF-Connecting-IP;
}