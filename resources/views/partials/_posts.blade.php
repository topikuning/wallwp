@foreach($listing as $row)
	<?php
		$url = url('wp-content/uploads/thumb/'. $row->slug . '.jpg');
	?>
	<article id="post-12" class="post type-post status-publish format-standard has-post-thumbnail sticky hentry category-uncategorized">
		<a class="post-thumbnail" href="{{ url($row->slug.'-'.$row->id.'.html') }}">
			<img src="{{ $url }}" alt="{{ $row->keyword.' - '.$row->title }}" />
		</a>
		<header class="entry-header">
			<h2 class="entry-title">
				<a href="{{ url($row->slug.'-'.$row->id.'.html') }}" rel="bookmark">{{ $row->title }}</a>
			</h2>
		</header><!-- .entry-header -->
	</article>
@endforeach