<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	@if(!isset($current_title))
        <title>{{ config('site.site_title') }}</title>
    @else
        <title>{{ $current_title }} | {{ config('site.site_title') }}</title>
    @endif
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
	<![endif]-->

	<link rel="alternate" type="application/rss+xml" title="WordPress &raquo; Feed" href="/feed" />

	<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C900%2C300italic%2C400italic%2C700italic&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
	<link rel='stylesheet' href="{{ url('/wp-content/themes/thedarkknight/genericons/genericons.css?ver=3.0.3')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ url('/wp-content/themes/thedarkknight/css/font-awesome-4.4.0/css/font-awesome.min.css')}}" type='text/css' media='all' />
	<link rel='stylesheet' href="{{ url('/wp-content/themes/thedarkknight/style.css?ver=4.4.2')}}" type='text/css' media='all' />
	<!--[if lt IE 9]>
	<link rel='stylesheet' id='thedarkknight-ie-css'  href="{{ url('/wp-content/themes/thedarkknight/css/ie.css?ver=20131205')}}" type='text/css' media='all' />
	<![endif]-->
	<link rel='https://api.w.org/' href='http://wp.dev/wp-json/' />
	<meta name="generator" content="WordPress 4.4.2" />
	@if(config('site.webmaster_id') !== "")
		<meta name="google-site-verification" content="{{ config('site.webmaster_id') }}" />
	@endif

	@if(config('site.bing_id') !== "")
		<meta name="msvalidate.01" content="{{ config('site.bing_id') }}" />
	@endif

	@if(!isset($no_description))
		@if(!isset($current_description))
			<meta name="description" content="{{ config('site.site_desc') }}">
		@else
			<meta name="description" content="{{ $current_description }}">
		@endif
	@endif

	@if(isset($noindex))
		<meta name="robots" content="noindex,follow" />
	@else
		<meta name="robots" content="index,follow" />
	@endif
	
	<script type="text/javascript">
        var base_url = "{{ url('/') }}";
    </script>
</head>