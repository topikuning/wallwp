@foreach($listing as $row)
	<?php
		$url = url('wp-content/uploads/thumb/'. $row->slug . '.jpg');
	?>
	<div class="item-wrap">
		<a href="{{ url($row->slug.'-'.$row->id) }}.html">
			<img src="{{ $url }}" alt="{{ htmlspecialchars($row->keyword) }}" />
		</a>

		<p class="title"><a href="{{ url($row->slug.'-'.$row->id) }}.html">{{ htmlspecialchars($row->title) }}</a></p>
	</div>
@endforeach