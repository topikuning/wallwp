<header id="masthead" class="site-header" role="banner">
	<div class="header-main">
		<p class="site-title"><a href="/" rel="home">{{ config('site.site_title') }}</a></p>

		<div class="search-toggle">
			<a href="#search-container" class="screen-reader-text" aria-expanded="false" aria-controls="search-container">Search
		</div>

		<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
			<button class="menu-toggle">Primary Menu</button>
			<a class="screen-reader-text skip-link" href="#content">Skip to content</a>
			<div id="primary-menu" class="nav-menu">
				<ul>
					@foreach($pages as $page)
						<li class="page_item page-item-6"><a href="{{ url('page/'. $page->slug) }}.html">{{ $page->title }}</a></li>
					@endforeach
				</ul>
			</div>
		</nav>
	</div>

	<div id="search-container" class="search-box-wrapper hide">
		<div class="search-box">
			<form role="search" method="get" class="search-form" action="/search">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="Search and hit Enter &hellip;" value="" name="s" title="Search for:" />
				</label>
					<input type="submit" class="search-submit" value="Search" />
			</form>
		</div>
	</div>
</header>