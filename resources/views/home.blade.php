<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="en">
<!--<![endif]-->
    @include('partials._header')
    <body class="home blog logged-in masthead-fixed list-view full-width grid">
        <div id="page" class="hfeed site">
            @include('partials._navbar')
			<div id="main" class="site-main">
			<div class="overlay-search"></div>
				@yield('content')
			</div>
        </div>
        @include('partials._footer')
    </body>
</html>