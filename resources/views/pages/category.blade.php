@extends('home')

@section('content')

	<div id="main-content" class="main-content">
		<header class="page-header">
			<h1 class="page-title">{{ $category }}</h1>
		</header><!-- .page-header -->

		<div id="featured-content" class="featured-content">
			@if(config('site.enable_ads'))
				<hr/>
				<div align="center">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- ATAS -->
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<hr/>
			@endif
			<div class="featured-content-inner">
				<?php $listing = $posts; ?>
				@include('partials._posts')
			</div>
		</div>
		@if(config('site.enable_ads'))
			<div align="center">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- BAWAH -->
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
					 data-ad-format="auto"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		@endif
	</div><!-- #main-content -->
@endsection
