@extends('home')
@section('content')

    <?php
        $url = url('wp-content/uploads/'. $post->slug . '.jpg');
    ?>
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $current_title }} - {{ config('site.site_title') }}" />
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:site_name" content="{{ config('site.site_title') }}" />
    <meta property="og:description" content="{{ ucfirst(strtolower(implode(' ', $body))) }}"/>
    <meta property="og:image" content="{{ $url }}" />
	
	<div id="primary" class="content-area clearfix">
		<div id="content" class="site-content" role="main">

      <article id="post" class="post type-post status-publish format-standard has-post-thumbnail hentry">
		@if(config('site.enable_ads'))
			@if(!$robot)
				<hr/>
				<div align="center">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						@if(!$mobile)
						 style="display:block"
						 data-ad-format="auto"
						@else
							style="display:inline-block;width:250px;height:250px"
						@endif
						 data-ad-client="{{config('site.ad_client')}}"
								 data-ad-slot="{{config('site.ad_slot')}}"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<hr/>
			@endif
		@endif
        <div class="post-thumbnail-wrap">
          <div class="post-thumbnail" aria-hidden="true">
            <img src="{{ $url }}" class="attachment-thedarkknight-full-width size-thedarkknight-full-width wp-post-image" alt="{{ $post->keyword .' - '. $post->title }}">
          </div>

          <header class="entry-header">
            <h1 class="entry-title">{{ $post->title }}</h1>
          </header><!-- .entry-header -->
		@if(config('site.enable_ads'))
			@if(!$mobile && !$robot)
				<hr/>
				<div align="center">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="{{config('site.ad_client')}}"
								 data-ad-slot="{{config('site.ad_slot')}}"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<hr/>
			@endif
		@endif
        </div><!-- .post-thumbnail-wrap -->

        <div class="content-text-wrap">
			<div class="entry-meta entry-details">
			<h2 class="widget-title">Tagged As.</h2>
                        <?php $tags = $details; $lkey="";?>
                        @if($lkey != $post->slug_keyword)
                                <span class="tag-links"><a rel="follow,index" href="{{ url('category/'.$post->slug_keyword.'.html') }}">{{ $post->keyword }}</a></span>
                                <?php $lkey = $post->slug_keyword;?>
                        @endif
                        @foreach($tags as $tag)
				<span class="tag-links"><a rel="follow,index" href="{{ url($tag->slug.'-'.$tag->id.'.html') }}">{{ $tag->title }}</a></span>
                                @if($lkey != $tag->slug_keyword)
                                        <span class="tag-links"><a rel="follow,index" href="{{ url('category/'.$tag->slug_keyword.'.html') }}">{{ $tag->keyword }}</a></span>
                                        <?php $lkey = $tag->slug_keyword;?>
                                @endif
                        @endforeach
		</div>
			@if(config('site.enable_ads'))
				@if($mobile && !$robot)
				<hr/>
				<div align="center">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:inline-block;width:300px;height:250px"
						 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<hr/>
				@endif
			@endif
          <div class="entry-details">
			  <h2 class="widget-title nomt">Download.</h2>
			  <ul class="photo-details">
				<li>
				  <div class="device-title">iPhone / Android</div>
				  <ul class="list list-inline list-icons">
					<li><a href="{{ url('download/320/480/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">320x480</a></li>
					<li><a href="{{ url('download/640/960/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">640x960</a></li>
					<li><a href="{{ url('download/640/1136/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">640x1136</a></li>
				  </ul>
				</li>

				<li>
				  <div class="device-title">iPad / Tablet</div>
				  <ul class="list list-inline list-icons">
					<li><a href="{{ url('download/1024/1024/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">1024x1024</a></li>
					<li><a href="{{ url('download/640/960/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">640x960</a></li>
					<li><a href="{{ url('download/640/1136/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">640x1136</a></li>
				  </ul>
				</li>

				<li>
				  <div class="device-title">Desktop</div>
				  <ul class="list list-inline list-icons">
					<li><a href="{{ url('download/1024/768/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">1024x768</a></li>
					<li><a href="{{ url('download/1152/864/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">1152x864</a></li>
					<li><a href="{{ url('download/1280/960/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">1280x960</a></li>
					<li><a href="{{ url('download/1600/1200/'. $post->code) }}" class="playlist-button" rel="nofollow,noindex" target="_blank">1600x1200</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- .entry-details -->
			
          <div class="entry-details">
			  <h2 class="widget-title">Custom Dimensions.</h2>
			  <form class="form-horizontal" target="_blank" action="/rsz" method="get">
				<div class="form-group">
				  <label for="w" class="col-sm-2 control-label">Width</label>
				  <div class="col-sm-10">
					<input type="text" name="w" id="w" class="form-control">
				  </div>
				</div>

				<div class="form-group">
				  <label for="h" class="col-sm-2 control-label">Height</label>
				  <div class="col-sm-10">
					<input type="text" name="h" id="h" class="form-control">
					<input type="hidden" name="c" id="c" value="{{$post->code}}">
				  </div>
				</div>

				<div class="form-group">
				  <label for="h" class="col-sm-2 control-label">&nbsp;</label>
				  <div class="col-sm-10">
					<button type="submit" class="btn btn-default btn-primary">Download</button>
				  </div>
				</div>
			  </form>
			</div><!-- .entry-details -->
        </div><!-- .content-text-wrap -->

        <div class="social-shares social-icons">
          <a onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-facebook"></i></a>
		  <a onclick="window.open('https://twitter.com/home?status=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-twitter"></i></a>
		  <a onclick="window.open('https://plus.google.com/share?url=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-google"></i></a>
		  <a onclick="window.open('http://pinterest.com/pin/create/button/?url=' + location.href, 'sharer', 'width=626,height=436');" href="#" rel="noindex,nofollow" class="social-link" target="_blank"> <i class="fa fa-pinterest"></i></a>
        </div>
        
      </article><!-- #post-## -->


       @include('partials._related')

		</div><!-- #content -->
	</div><!-- #primary -->
		
		
@endsection
