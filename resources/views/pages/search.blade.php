@extends('home')
@section('content')
<div id="main-content" class="main-content">

  <header class="page-header">
    <h1 class="page-title">Search Results for <?php echo filter_input(INPUT_GET, 's') ?></h1>
  </header><!-- .page-header -->

  <div id="featured-content" class="featured-content">
	@if(config('site.enable_ads'))
		<hr/>
		<div align="center">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- ATAS -->
			<ins class="adsbygoogle"
				 style="display:block"
				 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
				 data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
		<hr/>
	@endif
	<div class="featured-content-inner">
		<?php $listing = $posts; ?>
		@include('partials._posts')
	</div><!-- .featured-content-inner -->
	@if(config('site.enable_ads'))
		<div align="center">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- ATAS -->
			<ins class="adsbygoogle"
				 style="display:block"
				 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
				 data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	@endif
</div><!-- #featured-content .featured-content -->

	<nav class="navigation paging-navigation" role="navigation">
		<div class="pagination loop-pagination">
			<?php
				$ispage = $posts->currentPage();
				$prev = $ispage -1;
				$next = $ispage + 1;
			?>
			@if($ispage > 1)
				<a class="prev page-numbers" href="{{ url('/search?s='. urlencode($search) .'&page='. $prev) }}">&#171; Previous</a>
			@endif
			@if($posts->hasMorePages())
				<a class="next page-numbers" href="{{ url('/search?s='. urlencode($search) .'&page='. $next) }}">Next &#187;</a>
			@endif
		</div><!-- .pagination -->
	</nav>

</div><!-- #main-content -->
@endsection
