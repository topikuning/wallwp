@extends('home')

@section('content')
	
	<div id="main-content" class="main-content">
		<header class="page-header">
			<h1 class="page-title">{{ config('site.main_title') }}</h1>
		</header><!-- .page-header -->

		<div id="featured-content" class="featured-content">
			@if(config('site.enable_ads'))
				<hr/>
				<div align="center">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- ATAS -->
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<hr/>
			@endif
			<div class="featured-content-inner">
				@foreach($last_viewed as $viewed)
					<?php $url = url('wp-content/uploads/thumb/'. $viewed->slug . '.jpg'); ?>				
					<article id="post-12" class="post type-post status-publish format-standard has-post-thumbnail sticky hentry category-uncategorized">
						<a class="post-thumbnail" href="{{ url('category/'.$viewed->slug_keyword) }}.html">
							<img src="{{ $url }}" alt="{{ $viewed->keyword }}" />
						</a>
						<header class="entry-header">
							<h2 class="entry-title">
								<a href="{{ url('category/'.$viewed->slug_keyword) }}.html" rel="bookmark">{{ $viewed->keyword }}</a>
							</h2>
						</header><!-- .entry-header -->
					</article>
				@endforeach
			</div>
		</div>
		@if(config('site.enable_ads'))
			<div align="center">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- ATAS -->
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="{{config('site.ad_client')}}"
							 data-ad-slot="{{config('site.ad_slot')}}"
					 data-ad-format="auto"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		@endif
	</div><!-- #main-content -->

@endsection