
<?php
    $url = url('wp-content/uploads/'. $post->slug.'-'.$post->code . '.jpg');
?>
<a href="{{ url($post->slug.'-'.$post->id) }}.html" title="{{ $post->title }}">
    <img width="500" src="{{ $url }}" alt="{{ $post->title }}" />
</a>