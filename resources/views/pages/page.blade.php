@extends('home')
@section('content')

<div id="main-content" class="main-content">

	<header class="page-header">
		<h1 class="page-title">{{ $page->title }}</h1>
	</header><!-- .page-header -->

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<article id="post" class="post type-post status-publish format-standard has-post-thumbnail hentry">
				<div class="entry-content">
					<p>{!! str_replace('nama.situs',config('site.site_url'),$page->body) !!}</p>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->
		</div><!-- #content -->
	</div><!-- #primary -->
</div>

@endsection